package com.example;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.reactivex.Maybe;

@Controller
public class HelloWorld {
    @Get("/echo/{s}")
    public String echo(String s) {
        String test = new String("not used string");
        String test2 = new String("not2 used string");
        if(test == test) {
            return test;
        }
        if(s == test) {
            return test;
        } else {
            return s;
        }
    }
}
